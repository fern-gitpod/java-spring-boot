package com.example.demo.controllers;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;

@RestController
public class IndexController {

    private final ResourceLoader resourceLoader;

    @Autowired
    public IndexController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @GetMapping("/")
    public String index() {
        try {
            Resource resource = resourceLoader.getResource("classpath:index.html");
            String content = new String(Files.readAllBytes(resource.getFile().toPath()), StandardCharsets.UTF_8);
            return content;
        } catch (Exception e) {
            // Handle exception here
            e.printStackTrace();
            return "Error loading file";
        }
    }
}